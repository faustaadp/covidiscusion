from django import forms
from .models import Kasus

class FormKasus(forms.ModelForm):
    class Meta:
        model = Kasus
        fields = "__all__"
        widgets = {
            'jumlah' : forms.NumberInput(attrs={'class': 'form-control'}),
            'prov' : forms.Select(attrs={'class': 'form-control'}),
            'kotkab' : forms.TextInput(attrs={'class': 'form-control'}),
        }