# CoviDiscussion

[![pipeline status](https://gitlab.com/faustaadp/covidiscusion/badges/master/pipeline.svg)](https://gitlab.com/faustaadp/covidiscusion/-/commits/master)

[![coverage report](https://gitlab.com/faustaadp/covidiscusion/badges/master/coverage.svg)](https://gitlab.com/faustaadp/covidiscusion/-/commits/master)

## Kelompok B03

Nama                                 | NPM
------------------------------------ | -------------
Dzul Fiqar Aditya Widhiartanto       | 1906298853 
Raden Fausta Anugrah Dianparama      | 1906285560
Sonia Rahmawati                      | 1906398654

## CoviDiscussion
CoviDiscussion merupakan sebuah web dimana orang orang bisa berdiskusi, berpendapat, serta menginformasikan beberapa informasi dan pengalaman selama menghadapi pandemi Covid-19. Seperti yang kita tahu, pandemi ini membuat semuanya menjadi berbeda. Banyak hal yang biasa kita lakukan menjadi tidak bisa dilakukan, seperti bekerja, bersekolah, kuliah, berkumpul dengan teman-teman, dan banyak hal lain. Kemudian, ada hal-hal yang dulunya jarang dilakukan orang yang sekarang justru menjadi trend selama masa pandemi, seperti penggunaan layanan online meeting, webinar, dan masih banyak lagi. Semua pengalaman itu bisa diceritakan untuk menjadi inspirasi serta pembelajaran untuk orang-orang dalam menghadapi pandemi.

Kita dapat berbagi pengalaman dalam menjalani WFH, SFH, PJJ, atau kegiatan lain dari rumah. kita dapat berbagi tips-tips mengenai cara pencegahan agar tidak tertular Covid-19. Dan kita dapat berbagi informasi mengenai penyebaran covid-19 di lingkungan sekitar. Ini dilakukan agar kita dapat saling berdiskusi dan menemukan solusi terbaik dalam menghadapi Pandemi Covid-19

Fitur Fitur :

- Tampilan informasi tentang Covid-19
- Form mengabarkan adanya kasus di sekitar kita
- Informasi protokol kesehatan serta tips hidup sehat dan bersih selama Pandemi
- Form untuk usulan terkait tips hidup sehat dan protokol kesehatan
- Halaman diskusi berisi pengalaman orang-orang dalam menghadapi Pandemi serta saat menjalankan kegiatan dari rumah


## High Fidelity Prototype
https://www.figma.com/file/ECMII2nYhBRe84Nta0tfCy/TK1-PPW-B03?node-id=21%3A2

## Persona
https://www.figma.com/file/pp8N1d4Ww7HEPvLzV29QdO/Untitled?node-id=0%3A1

## Wireframe

#### Halaman Utama
https://wireframe.cc/thUVA7

#### Kasus
https://wireframe.cc/X0siU7

#### Pengalaman
https://wireframe.cc/shxPCQ



## Link Heroku

http://covidiscussion.herokuapp.com/

