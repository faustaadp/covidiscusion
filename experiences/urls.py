from django.urls import path

from .views import experiences, saveexperience, deleteexperience

app_name = 'experiences'

urlpatterns = [
    path('', experiences, name='experiences'),
    path('saveexperience', saveexperience, name='saveexperience' ),
    path('deleteexperience/<str:pk>', deleteexperience, name='deleteexperience'),
]
