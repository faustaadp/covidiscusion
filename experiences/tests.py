from django.test import TestCase
from django.urls import resolve
from .views import experiences, saveexperience, deleteexperience
from .models import Experience

# Create your tests here.
class ExperiencesTest(TestCase):
    def setUp(self):
        experience = Experience.objects.create(nama="Bambang", lokasi="Jagakarsa", pengalaman="test1")

    def test_url_experiences(self):
        response = self.client.get('/experiences/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'experiences/experiences.html')

    
    def test_views_use_experiences(self):
        found = resolve('/experiences/')
        self.assertEqual(found.func, experiences)

    def test_save_experience(self):
        response = self.client.post('/experiences/saveexperience', data={'nama' : 'Anji', 'lokasi' : 'Cisauk', 'pengalaman' : 'test2'})
        self.assertEqual(Experience.objects.all().count(), 2)
        self.assertEqual(Experience.objects.get(id=2).nama, "Anji")
        self.assertEqual(Experience.objects.get(id=2).lokasi, "Cisauk")
        self.assertEqual(Experience.objects.get(id=2).pengalaman, "test2")
        
        

    def test_delete_experience(self):
        response = self.client.post('/experiences/deleteexperience/1')
        self.assertEqual(Experience.objects.all().count(), 0)