from django.db import models

# Create your models here.
class Experience(models.Model):
    nama = models.CharField(max_length = 50)
    lokasi = models.CharField(max_length = 100)
    pengalaman = models.TextField()